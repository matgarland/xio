################################################################################
#
# 	program name:	xio
#
#	version: 		1.1.4
#	author: 		matg74@gmail.com
# 	description:    execute supplied command, display that command and
#	its associated output in the terminal and print the command executed
#	and it's output to an external file in append mode.
#
#	execution format: xio ls -lah, where ls -lah is the command to be
#	executed. the directory contents shall be displayed in the terminal,
#	as well as the command executed printed to the defined output file,
#	along with the directory listing printed to the output file.
#
################################################################################
################################################################################
#
# 	__xio_get_file_path
#
#	description:	__xio_get_file_path is a "private" convenience function,
#	retrieving the user's ~/.profile held value for the XIO_OUT_PATH variable.
#
################################################################################
__xio_get_file_path() {
	# obtain the value of XIO_OUT_PATH from the user's ~/.profile
    OUTPUT_PATH=$( grep "export XIO_OUT_PATH=" ~/.profile | sed 's/export\| XIO_OUT_PATH=//g' )
    echo "${OUTPUT_PATH}"
}
################################################################################
#
# 	xio
#
#	description:	execute supplied command, display that command and
#	its associated output in the terminal and print the command executed
#	and it's output to an external file in append mode.
#
################################################################################
xio()
{	
    # if number of command arguments is not equal to zero, continue execution
    if [[ "$#" -ne 0 ]]; then
    { 
        # special use case of a user supplied exit command which otherwise doesn't get handled well by xio
        if [[ "$1" = 'exit' ]]; then
		{
	    	echo "xio is unable to process the exit command."
		}
		fi
	    
		# if the output path isn't set (See: xioset()), write to default out.log on the Desktop
		if [[ -z "${XIO_OUT_PATH}" ]]; then
		{
			# notify the user that the output path isn't set, and that we are outputting to a default file.
			echo -e "XIO_OUT_PATH is not set, outputting to default output file, /home/${USER}/Desktop/xio_out.log";
			( echo "COMMAND EXECUTED: $@" && $@ ) |& tee -a "/home/${USER}/Desktop/xio_out.log";
		}
		# XIO_OUT_PATH is set, echo the executed command and result as well as output to XIO_OUT_PATH.
		else
		{
			( echo "COMMAND EXECUTED: $@" && $@ ) |& tee -a "${XIO_OUT_PATH}";
		}
		fi
	}
	# else, the number of command arguments equals zero so notify the user of command usage.
	else
	{
		echo -e "The xio command requires at least one argument to execute.\n"
		echo -e "The xio command format is \"xio [command] [arguments]\"\n";
		echo -e "Example: \"xio ls -lah\"\n";
	}
    fi
}

################################################################################
#
# 	xioset
#
#	description:	xioset is a convenience function enabling the user to set
#	a new value for the .profile variable XIO_OUT_PATH, effectively
#	allowing the user to choose the location and file name of the command
#	output file thereby persisting across sessions & restarts the chosen path.
#
################################################################################
xioset()
{
    # if function arguments supplied is not equal to 1 argument, supply somewhat of a command usage statement.
    if [[ "$#" -ne 1 ]]; then
    {
		echo -e "xioset expects a single command argument, a named file, which is to be used to record the output of the xio command!"
    }
    fi

    # if there is only 1 argument supplied
    if [[ "$#" -eq 1 ]]; then
    {
		# var to hold the specified out file argument
		SPECD_FILE=${1}
		# create var to hold full text prior to proper escaping
		REPLACE_TEXT="export XIO_OUT_PATH=${SPECD_FILE}"
		# escaped version of exported output file path variable
		REPLACE_ESCAPED=$(echo "${REPLACE_TEXT}" | sed 's_/_\\/_g' )

		# check to see if the XIO_OUT_PATH already exists in the user's ~/.profile file
		# grep's result will be "0:export XIO_OUT_PATH....", 0 meaning fail
		EXISTS="$(grep -n XIO_OUT_PATH /home/${USER}/.profile)"
		# check the first character of EXISTS grep result to retrieve the grep outcome
		if [[ "${EXISTS:0:1}" -eq 0 ]]; then
		{
			# no existing XIO_OUT_PATH var located in file
			# (append, so as not to delete other settings) a completely new line to the file
			( echo -e "${REPLACE_ESCAPED}" >> "/home/${USER}/.profile" )
		# if EXISTS first character equals 1, we know the XIO_OUT_PATH entry exists in the user's ~/.profile
		} else {
			# delete the line from the user's ~/.profile which contains the variable XIO_OUT_PATH
			DELETED="$( sed '/XIO_OUT_PATH=*/d' /home/${USER}/.profile )"
			# if the line was deleted, (first char indicating success equal to 1)
			if [[ "${DELETED:0:1}" -eq 1 ]]; then
			{
				# simply create another line entry in the user's ~/.profile for the updated XIO_OUT_PATH entry
				( echo -e "${REPLACE_ESCAPED}" >> "/home/${USER}/.profile" )
			}
			# else, if nothing was deleted, we must stream edit the ~/.profile and replace the matching line
			else
			{
				# sed -i requires GNU sed and is not portable
				# ref https://stackoverflow.com/questions/5410757/delete-lines-in-a-text-file-that-contain-a-specific-string
				sed -i "s/export XIO_OUT_PATH=.*/${REPLACE_ESCAPED}/g" "/home/${USER}/.profile"
			}
			fi
		}
    	fi
		# source the user's profile to enable XIO_OUT_PATH to be persisted between sessions and reboot
		source "/home/${USER}/.profile"
		# advise user of setting made.
		echo -e "xio out file set to: $SPECD_FILE\n"
    }
    fi
}

################################################################################
#
# 	xioget
#
#	description:	xioget is a convenience function enabling the user to get
#	the current value of the user's .profile variable, XIO_OUT_PATH.
#
################################################################################
xioget() {
    # if arguments supplied is not exactly 1 argument, supply somewhat of a command usage statement.
    if [[ "$#" -ne 0 ]]; then
    {
        echo -e "xioget expects no command arguments, it fulfills the read-only get function of the xio command!"
    }
    else
    {
    	# obtain the value of XIO_OUT_PATH from the user's .profile
    	OUTPUT_PATH=`__xio_get_file_path`
        echo -e "${OUTPUT_PATH}"
    }
    fi
}

