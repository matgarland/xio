# xio  
## What is xio?  
xio is a bash script which can be included into the ~/.bashrc of your Ubuntu operating system, it facilitates command execution with result output to the terminal and to a specified output file.

## Dependencies  
xio has the following dependencies:
- GNU bash version 4+;
- the use of `sed -i` necessitates GNU sed.

## Included Functions  
### xio  

Execute the supplied command, redirecting output to both the terminal and an out file specified by the xioset command.  

Command format:  
```
    $ xio ls -lah
```

### xioset  

Set the named output file for commands executed using the xio prefix.  

Command format:  
```
    $ xioset ~/Desktop/outfile.log
```

### xioget  

xioget provides a quick sanity check for users to get the location and name of the file to where output is being directed.  

Command format:  
```
    $ xioget
```

## License  

MIT License

Copyright (c) 2019 Mathew Garland

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


